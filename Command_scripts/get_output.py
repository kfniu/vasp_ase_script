#!/usr/bin/env python3

from ase.io import read
import sys

files = sys.argv[1].lstrip()

#print (atoms.get_potential_energy())
if sys.argv[2].lstrip() == 'E':
   atoms = read(files)
   print (atoms.get_potential_energy())
elif sys.argv[2].lstrip() == 'loop':
   atoms = read('%s@:' % files)
   print (len(atoms[:]))
else:
   print ('input not supported!')
