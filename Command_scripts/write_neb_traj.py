#!/usr/bin/env python3

# from ase.all import *
from ase.io import read, write
import ase
from ase.calculators.vasp import xdat2traj
import pickle
import os
import sys

name = sys.argv[1]

atoms0 = read('00/POSCAR')
scaled0 = atoms0.get_scaled_positions(wrap=False)
#out = ase.io.trajectory.PickleTrajectory(name, mode='w')
#out.write_header(atoms_0)

for line in open('INCAR').readlines():
    if line.find('IMAGES') >= 0:
        N_images = int(line.split('=')[-1])

images = []
for n in range(1,1+N_images):
    os.chdir('%02d' % n)
    os.system('ln -s ../INCAR .')
    os.system('ln -s ../POTCAR .')
    os.system('ln -s ../KPOINTS .')
    os.system('write_traj.py tmp.traj')
    atoms = read('tmp.traj@-1')
    # Move atoms to same side of cell
    scaled = atoms.get_scaled_positions(wrap=False)
    for m in range(len(scaled)):
        for i in range(3):
            if scaled[m,i] - scaled0[m,i] > 0.5:
                scaled[m,i] -= 1.
            elif scaled[m,i] - scaled0[m,i] < -0.5:
                scaled[m,i] += 1.
    atoms.set_scaled_positions(scaled)
    # Redefine calculators atoms, otherwise it will think coordinates have been updated.
    atoms.calc.atoms = atoms
    images += [atoms]
    os.system('rm tmp.traj INCAR POTCAR KPOINTS')
    os.chdir('../')

write(name, images)


