#! /usr/bin/env python3

from ase.io import read
from ase.visualize import view
from ase.neb import NEB
import sys

n = len(sys.argv)

ini_state = sys.argv[1]
fin_state = sys.argv[-1]

IS = read(ini_state)
FS = read(fin_state)

if n == 3:
	number_of_images = input('number of images:')

	images = [IS]
	images += [IS.copy() for i in range(int(number_of_images))]
	images += [FS]

	neb = NEB(images)
	neb.interpolate('idpp')
	view(images)
	print ('done!')
elif n == 4:
	n_part1 = input('number of the first part:')
	n_part2 = input('number of the second part:')
	
	insert_state = sys.argv[2]
	IntS = read(insert_state)

	images1 = [IS]
	images1 += [IS.copy() for i in range(int(n_part1))]
	images1 += [IntS]

	neb1 = NEB(images1)
	neb1.interpolate('idpp')

	images2 = [IntS]
	images2 += [IntS.copy() for i in range(int(n_part2))]
	images2 += [FS]

	neb2 = NEB(images2)
	neb2.interpolate('idpp')

	images = images1 + images2[1:]
	view(images)
	print ('done!')
elif n == 5:
	n_part1 = input('number of the first part:')
	n_part2 = input('number of the second part:')
	n_part3 = input('number of the third part:')

	insert_state1 = sys.argv[2]
	insert_state2 = sys.argv[3]
	IntS1 = read(insert_state1)
	IntS2 = read(insert_state2)

	images1 = [IS]
	images1 += [IS.copy() for i in range(int(n_part1))]
	images1 += [IntS1]

	neb1 = NEB(images1)
	neb1.interpolate('idpp')

	images2 = [IntS1]
	images2 += [IntS1.copy() for i in range(int(n_part2))]
	images2 += [IntS2]

	neb2 = NEB(images2)
	neb2.interpolate('idpp')

	images3 = [IntS2]
	images3 += [IntS2.copy() for i in range(int(n_part3))]
	images3 += [FS]

	neb3 = NEB(images3)
	neb3.interpolate('idpp')

	images = images1 + images2[1:-1] + images3[1:] 
	view(images)
	print ('done!')
elif n < 3:
	print ('No sufficient input structures!')
else:
	print ('Too many insertations!')




	


