#!/usr/bin/env python3

from ase.io import read
import sys

file_1 = sys.argv[1].lstrip()
file_2 = sys.argv[2].lstrip()

atoms0 = read(file_1)
scaled0 = atoms0.get_scaled_positions(wrap=False)

atoms = read(file_2)
scaled = atoms.get_scaled_positions(wrap=False)
for m in range(len(scaled)):
    for i in range(3):
        if scaled[m,i] - scaled0[m,i] > 0.9:
            scaled[m,i] -= 1.
        elif scaled[m,i] - scaled0[m,i] < -0.9:
            scaled[m,i] += 1.
atoms.set_scaled_positions(scaled)
atoms.write(file_2)

