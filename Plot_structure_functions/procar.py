import numpy as np
import sys
import os

class procar_data:
    """
    Class to read in data in the procar file

    Can specify filename, default is PROCAR
    number of spin channels, default is 1.
    Whether noncollinear calculation was performed, default is False

    The script was initially written by Matthew Dyer, University of Liverpool,
    but has been edited to support Python 3.
    """

    def __init__(self,filename='PROCAR', nspin=1, noncollinear=False, ase_sort=False):
        """
        Reads PROCAR file, including kpoints, band energies and site-projections
        """
        self.filename = filename
        self.nspin = nspin
        self.noncollinear = noncollinear
        if self.noncollinear: 
            print('NB: Separate x, y and z contributions are ignored for noncollinear calculations.')
            # Only one set of data if noncollinear calculation is performed
            self.nspin = 1
        #Open file object
        if os.path.isfile(self.filename):
            procar_file = open(self.filename,'r')
        else:
            print('No file with name %s was found.' % filename)
            sys.exit(0)
        #Read first line and determine file type
        line = procar_file.readline()
        if line.find('phase') != -1:
            # LORBIT = 12
            self.itype = 2
            print(' File is lm decomposed with phase information')
        elif line.find('lm decomposed') != -1:
            # LORBIT = 11
            self.itype = 1
            print(' File is lm decomposed without phase information')
        elif line.find("new format")!=-1:
            # LORBIT = 10
            self.itype = 0
            print(' File is l decomposed')
        else:
            print(' Could not recognise PROCAR file\n')
            sys.exit()
        #Read in number of k-points, bands and ions
        line = procar_file.readline()
        self.nkpoints = int(line.split()[3])
        self.nbands_up = int(line.split()[7])
        self.nions = int(line.split()[-1])
        print('    Number of k-points : %d' % self.nkpoints)
        print('       Number of bands : %d' % self.nbands_up)
        print('        Number of ions : %d' % self.nions)
        #Set up some arrays
        self.kpoints = np.zeros((self.nkpoints,3), np.float)
        self.weights = np.zeros((self.nkpoints), np.float)
        self.kpointr = np.zeros((self.nkpoints), np.float)
        self.energies = np.zeros((self.nspin,self.nkpoints,self.nbands_up), np.float)
        self.occupancies = np.zeros((self.nspin,self.nkpoints,self.nbands_up), np.float)
        if self.nspin == 1:
            self.nbands_down = None
        #Start loop over spins
        for ispin in range(self.nspin):
            if ispin == 1:
                line = procar_file.readline()
                if line=='':
                    print(' File only contains data for one spin channel.')
                    sys.exit(0)
                self.nbands_down = int(line.split()[-1]) # Test so this work.
                if self.nbands_down!=self.nbands_up:
                    print(' Warning: Different number of bands in the two spin channels. This has not been thoroughly tested.')
#                    sys.exit(0)
            # Data for up spin
            for ikpt in range(self.nkpoints):
                line = procar_file.readline()
                line = procar_file.readline()
                #Check we are at the right place
                if line.find('k-point') == -1:
                    print(' Error reading in PROCAR file')
                    print(' k-point %d' % (ikpt+1))
                    sys.exit(0)
                #Read k-point info
                line_split = line.split()
                # Some k-point values might be negative and this can mess up the script. Here is a fix.
                if len(line_split) == 8:
                    if (len(line_split[3].split('-')) == 2 and line_split[3][0] != '-') or len(line_split[3].split('-')) == 3:
                        if line_split[3][0] == '-':
                            new_dat1 = -float(line_split[3].split('-')[1])
                            new_dat2 = -float(line_split[3].split('-')[2])
                        else:
                            new_dat1 = float(line_split[3].split('-')[0])
                            new_dat2 = -float(line_split[3].split('-')[1])
                        line_split = line_split[:3] + [new_dat1, new_dat2] + line_split[4:]
                    else:
                        if line_split[4][0] == '-':
                            new_dat1 = -float(line_split[4].split('-')[1])
                            new_dat2 = -float(line_split[4].split('-')[2])
                        else:
                            new_dat1 = float(line_split[4].split('-')[0])
                            new_dat2 = -float(line_split[4].split('-')[1])
                        line_split = line_split[:4] + [new_dat1, new_dat2] + line_split[5:]
                elif len(line_split) == 7:
                    if line_split[3][0] == '-':
                        new_dat1 = -float(line_split[3].split('-')[1])
                        new_dat2 = -float(line_split[3].split('-')[2])
                        new_dat3 = -float(line_split[3].split('-')[3])
                    else:
                        new_dat1 = float(line_split[3].split('-')[0])
                        new_dat2 = -float(line_split[3].split('-')[1])
                        new_dat3 = -float(line_split[3].split('-')[2])
                    line_split = line_split[:3] + [new_dat1, new_dat2, new_dat3] + line_split[4:]

                self.kpoints[ikpt,:] = [line_split[n] for n in range(3,6)]
                self.weights[ikpt] = float(line_split[-1])
                #Distance along k-point line
                if ikpt > 0:
                    kdiff = np.sqrt(np.dot((self.kpoints[ikpt]-self.kpoints[ikpt-1]),(self.kpoints[ikpt]-self.kpoints[ikpt-1])))
                    self.kpointr[ikpt]=self.kpointr[ikpt-1]+kdiff

                # It is possible to sort the order of the ions according to ASE
                if ase_sort:
                    sort_ions = np.loadtxt('ase-sort.dat', np.int).T[0]
                else:
                    sort_ions = range(self.nions)
                if self.nions != len(sort_ions):
                    print('Sort vector has the wrong size!')
                
                #Read in band energy and occupancy
                for iband in range(self.nbands_up):
                    line = procar_file.readline()
                    line = procar_file.readline()
                    #Check we are at the right place
                    if line.find('band') == -1:
                        print(' Error reading in PROCAR file')
                        print(' k-point %d, band %d' % (ikpt+1,iband+1))
                        sys.exit(0)
                    self.energies[ispin,ikpt,iband] = float(line.split()[4])
                    self.occupancies[ispin,ikpt,iband] = float(line.split()[-1])
                    # Read in projections
                    line = procar_file.readline()
                    line = procar_file.readline()
                    # Work out number of projections
                    if ispin == 0 and ikpt == 0 and iband == 0:
                        self.nproj = len(line.split()) - 2
                        self.projections = np.zeros((self.nspin, self.nkpoints, self.nbands_up, self.nions, self.nproj), np.float16)
                        self.phases = np.zeros((self.nspin, self.nkpoints, self.nbands_up, self.nions, self.nproj, 2), np.float16)
                    for iion in sort_ions:
                        line = procar_file.readline()
                        #for iproj in range(self.nproj):
                        self.projections[ispin, ikpt, iband, iion, :] = [float(line.split()[n]) for n in range(1, self.nproj+1)]
                    #Skip totals line if more than one ion present
                    if self.nions>1:
                        line = procar_file.readline()
                    #If it is a noncollinear spin calculation we will skip the lines with separate contributions
                    if self.noncollinear:
                        for i in range(3*(self.nions+1)):
                            line = procar_file.readline()
                    if self.itype == 2:
                        line = procar_file.readline()
                        for iion in sort_ions:
                            line = procar_file.readline()
                            for iproj in range(self.nproj):
                                self.phases[ispin, ikpt, iband, iion, iproj, 0] = float(line[4+iproj*7:10+iproj*7])
                            line = procar_file.readline()
                            for iproj in range(self.nproj):
                                self.phases[ispin, ikpt, iband, iion, iproj, 1]=float(line[4+iproj*7:10+iproj*7])
                    # If f-functions are present this seems to be necessary
                    if self.nproj==16:
                        line = procar_file.readline()
                line = procar_file.readline()

        print(' Successfully read file %s' % self.filename)

    def get_dos(self, emin, emax, nepoints, width, energy_shift = 0., lineshape = 'Gaussian', spins=[]):
        """
        Returns the total density of states

        Returns the density of states as a numpy array over a given range by broadening
        states with a specified line shape 
        """
        
        # Check lineshape is supported
        # Currently accepts:
        #   gaussian - for Gaussian function
        #   erf - for error function
        #   lorentzian - for Lorentzian function
        #   gausslorentz - for a sum of Gaussian and Lorentzian functions
        if lineshape.lower()=='gaussian':
            # Redefine width so that full-width at half maximum is correct
            width = width / 2. / np.sqrt(2.*np.log(2.))
        elif lineshape.lower()=='erf':
            # Import erf function from scipy
            from scipy.special import erf
            # Redefine width so that full-width at half maximum is correct
            width = width / 2. / np.sqrt(2.*np.log(2.))
        elif lineshape.lower()=='gausslorentz':
            # Define Gaussian width parameter so that full-width at half maximum is correct
            sigma = width / 2. / np.sqrt(2.*np.log(2.))
        elif lineshape.lower()!='lorentzian':
            print(' ERROR: Line shape %s is not currently supported.' % (lineshape))
            return
        # If spins is empty then run over all spin channels present
        if spins==[]:
            spins = range(self.nspin)
        dos = np.zeros((nepoints,2), np.float16)
        # Work out difference between energy points
        ediff = (emax-emin) / float(nepoints-1)
        print(' Started calculation of density of states.')
        # Loop over points in energy
        if lineshape.lower() == 'gaussian':
            # Calculate gaussian prefactor and constant
            prefactor = 1. / width / np.sqrt(2.*np.pi)
            constant = 1. / 2. / width**2
            for ie in range(nepoints):
                energy = emin + ie*ediff
                dos[ie,0] = energy
                # Calculate array of gaussians, one element per band
                gaussian = np.exp(-constant*(self.energies[spins,...] + energy_shift - energy)**2)
                # Scale gaussian by prefactor and k-point weight
                gaussian = prefactor * gaussian
                weights = self.weights
                weights.shape = [1, self.nkpoints, 1]
                gaussian = weights*gaussian
                #Sum over all gaussian contributions at this energy
                dos[ie,1] = np.sum(gaussian)
        elif lineshape.lower() == 'lorentzian':
            #Calculate gaussian prefactor and constant
            prefactor = width / np.pi / 2.
            constant = (width / 2.)**2
            for ie in range(nepoints):
                energy = emin+ie*ediff
                dos[ie,0] = energy
                # Calculate array of lorentzians, one element per band
                lorentzian = 1./((self.energies[spins,...]+energy_shift-energy)**2+constant)
                # Scale lorentzian by prefactor and k-point weight
                lorentzian = prefactor*lorentzian
                weights = self.weights
                weights.shape = [1,self.nkpoints,1]
                lorentzian = weights*lorentzian
                # Sum over all lorentzian contributions at this energy
                dos[ie,1] = np.sum(lorentzian)
        elif lineshape.lower() == 'gausslorentz':
            g_prefactor = 1. / sigma / np.sqrt(2.*np.pi)
            g_constant = 1. / 2. / sigma**2
            l_prefactor = width / np.pi / 2.
            l_constant = (width/2.)**2
            for ie in range(nepoints):
                energy = emin+ie*ediff
                dos[ie,0] = energy
                #Calculate array of gaussians, one element per band
                gaussian = np.exp(-g_constant*(self.energies[spins,...]+energy_shift-energy)**2)
                #Scale gaussian by prefactor and k-point weight
                gaussian = g_prefactor*gaussian
                weights = self.weights
                weights.shape = [1,self.nkpoints,1]
                gaussian = weights*gaussian
                #Sum over all gaussian contributions at this energy
                dos[ie,1] = 0.5*np.sum(gaussian)
                #Calculate array of lorentzians, one element per band
                lorentzian = 1./((self.energies[spins,...]+energy_shift-energy)**2+l_constant)
                #Scale lorentzian by prefactor and k-point weight
                lorentzian = l_prefactor*lorentzian
                lorentzian = weights*lorentzian
                #Sum over all lorentzian contributions at this energy
                dos[ie,1] = dos[ie,1]+0.5*np.sum(lorentzian)
        else:
            constant = 1.0/(np.sqrt(2.0)*width)
            for ie in range(nepoints):
                energy = emin + ie*ediff
                estart = energy - ediff/2.0
                eend   = energy + ediff/2.0
                dos[ie,0] = energy
                #Calculate array of error functions, one element per band
                contribution = erf(constant*(self.energies[spins,...]+energy_shift-estart))-erf(constant*(self.energies[spins,...]+energy_shift-eend))
                weights = self.weights
                weights.shape = [1,self.nkpoints,1]
                contribution *= weights
                #Sum over all gaussian contributions at this energy
                dos[ie,1] = np.sum(contribution)/2.0/ediff  # factor 2 needed in transition to error function from gaussian
        print(' Completed calculation of density of states.')
        return dos

    def get_integrated_dos(self, emin, emax, nepoints, width, energy_shift=0.0, lineshape="Gaussian", spins=[]):
        """
        Returns the integrated total density of states

        Returns the density of states as a numpy array over a given range by broadening
        states with a specified line shape 
        """
        # If spins is empty then run over all spin channels present
        if spins==[]:
            spins=range(self.nspin)
        int_dos = self.get_dos(emin, emax, nepoints, width, energy_shift, lineshape,spins)
        ediff= (emax-emin) / float(nepoints-1)
        # Set dos at first energy to zero to initialise integration
        int_dos[0,1] = 0.
        # Integrate
        int_dos[:,1] = int_dos[:,1].cumsum()
        int_dos[:,1] = int_dos[:,1]*ediff
        return int_dos

    def get_partial_dos(self, emin, emax, nepoints, width, energy_shift=0., lineshape='Gaussian', spins=[], atoms=[], kpoints=[], orbitals='all'):
        """
        Returns the partial density of states

        Returns the partial density of states as a numpy array over a given energy range by broadening
        states with a specified line shape. This can be done for one atom, or summed over a number of atoms, 
        potentially even summed over specific k-points only.
        The atoms should be specified in atoms as indices starting from 0. 
        Different angular momentum can be specified using orbitals. Currently 'all', 's', 'p', 'px', 'py', 'pz',
        'sp', 'sp2', 'd', 'dxy', 'dxz', 'dyz', 'dz2' and 'dx2' are recognised.
        """
        # Check lineshape is supported
        if lineshape.lower() == 'gaussian':
            # Redefine width so that full-width at half maximum is correct
            width = width / 2. / np.sqrt(2.*np.log(2.))
        elif lineshape.lower()=='erf':
            # Import erf function from scipy
            from scipy.special import erf
            # Redefine width so that full-width at half maximum is correct
            width = width / 2. / np.sqrt(2.*np.log(2.))
        elif lineshape.lower() == 'gausslorentz':
            #Define Gaussian width parameter so that full-width at half maximum is correct
            sigma = width / 2./ np.sqrt(2.*np.log(2.))
        elif lineshape.lower()!='lorentzian':
            print(' ERROR: Line shape %s is not currently supported.' % (lineshape))
            return
        # If spins is empty then run over all spin channels present
        if spins == []:
            spins = range(self.nspin)
        # Determine k-point weights
        if kpoints==[]:
            kpoints = range(self.nkpoints)
            weights_temp = self.weights.copy()
        else:   # take relative k-point weight but normalize it only over those points selected ...
            wtotal = 0
            for i in kpoints: 
                wtotal += self.weights[i]
            weights_temp = np.zeros(self.nkpoints)    # unused k-points get zero weight ... 
            for i in kpoints: 
                weights_temp[i] = self.weights[i]/wtotal
        pdos = np.zeros((nepoints,2), np.float)
        # Work out difference between energy points
        ediff = (emax-emin)/float(nepoints-1)
        # Parse orbitals string to select which projections to sum over
        proj = self.parse_orbitals(orbitals)
        # If atoms is empty, run over all atoms
        if atoms == []:
            atoms = range(self.nions)
        print(' Started calculation of partial density of states.')
        # Reduce projections to just those needed
        temp_proj = self.projections[:,:,:,atoms,:]
        temp_proj = temp_proj[:,:,:,:,proj]
        temp_proj = temp_proj.sum(axis=4)
        temp_proj = temp_proj.sum(axis=3)
        temp_proj = temp_proj[spins,...]
        # Loop over points in energy
        if lineshape.lower() == 'gaussian':
            # Calculate gaussian prefactor and constant
            prefactor = 1. / width / np.sqrt(2.*np.pi)
            constant = 1./2./width/width
            for ie in range(nepoints):
                energy = emin + ie*ediff
                pdos[ie,0] = energy
                # Calculate array of gaussians, one element per band
                gaussian = np.exp(-constant*(self.energies[spins,...]+energy_shift-energy)**2)
                # Scale gaussian by prefactor and k-point weight
                gaussian = prefactor*gaussian
                weights_temp.shape = [1,self.nkpoints,1]
                gaussian = weights_temp*gaussian
                #Sum over all bands and projections
                pdos[ie,1] = np.sum(gaussian*temp_proj)
        elif lineshape.lower() == 'lorentzian':
            #Calculate gaussian prefactor and constant
            prefactor = width/np.pi/2.
            constant = (width/2.)**2
            for ie in range(nepoints):
                energy = emin+ie*ediff
                pdos[ie,0] = energy
                # Calculate array of lorentzians, one element per band
                lorentzian = 1./((self.energies[spins,...]+energy_shift-energy)**2+constant)
                # Scale lorentzian by prefactor and k-point weight
                lorentzian = prefactor*lorentzian
                weights=self.weights
                weights.shape=[1,self.nkpoints,1]
                lorentzian=weights*lorentzian
                # Sum over all lorentzian contributions at this energy
                pdos[ie,1] = np.sum(lorentzian*temp_proj)
        elif lineshape.lower() == 'gausslorentz':
            g_prefactor = 1. / sigma / np.sqrt(2.*np.pi)
            g_constant = 1. / 2. / sigma**2
            l_prefactor = width / np.pi / 2.
            l_constant = (width/2.)**2
            for ie in range(nepoints):
                energy = emin+ie*ediff
                pdos[ie,0] = energy
                # Calculate array of gaussians, one element per band
                gaussian = np.exp(-g_constant*(self.energies[spins,...]+energy_shift-energy)**2)
                # Scale gaussian by prefactor and k-point weight
                gaussian = g_prefactor*gaussian
                weights = self.weights
                weights.shape = [1,self.nkpoints,1]
                gaussian = weights*gaussian
                # Sum over all gaussian contributions at this energy
                pdos[ie,1] = 0.5*np.sum(gaussian*temp_proj)
                #Calculate array of lorentzians, one element per band
                lorentzian = 1./((self.energies[spins,...]+energy_shift-energy)**2+l_constant)
                #Scale lorentzian by prefactor and k-point weight
                lorentzian = l_prefactor*lorentzian
                lorentzian = weights*lorentzian
                #Sum over all lorentzian contributions at this energy
                pdos[ie,1] = pdos[ie,1]+0.5*np.sum(lorentzian*temp_proj)
        else:
            constant = 1.0/(np.sqrt(2.0)*width)
            for ie in range(nepoints):
                energy = emin+ie*ediff
                estart = energy-ediff/2.0
                eend = energy+ediff/2.0
                pdos[ie,0] = energy
                # Calculate array of error functions, one element per band
                contribution = erf(constant*(self.energies[spins,...]+energy_shift-estart))-erf(constant*(self.energies[spins,...]+energy_shift-eend))
                weights_temp.shape = [1,self.nkpoints,1]
                contribution *= weights_temp
                # Sum over all bands and projections
                pdos[ie,1] = np.sum(contribution*temp_proj)/2.0/ediff   # factor 2 needed in transition to error functions from gaussians
        print(' Completed calculation of partial density of states.')
        return pdos

    def get_projections(self, atoms=[], orbitals='all'):
        """
        Returns projections onto specific atoms and orbitals
        """
        # Parse orbitals string to select which projections to sum over
        proj = self.parse_orbitals(orbitals)
        # Reduce projections to just those needed
        temp_proj = self.projections[:,:,:,atoms,:]
        temp_proj = temp_proj[:,:,:,:,proj]
        #Sum over those atoms and orbitals
        return temp_proj.sum(axis=-1).sum(axis=-1)

    def parse_orbitals(self,orbitals):
        """
        Function to parse orbitals option
        """
        #Parse orbitals string to select which projections to sum over
        if self.itype==0 and len(orbitals)>1 and orbitals!='all' and orbitals!='sp':
            print('Orbital choice %s is not consistent with this PROCAR file' % orbitals)
            return
        if orbitals=='s':
            proj=[0]
        elif orbitals=='p':
            if self.itype==0:
                proj=[1]
            else:
                proj=[1, 2, 3]
        elif orbitals=='px':
            proj=[3]
        elif orbitals=='py':
            proj=[1]
        elif orbitals=='pz':
            proj=[2]
        elif orbitals=='sp':
            if self.itype==0:
                proj=[0, 1]
            else:
                proj=[0, 1, 2, 3]
        elif orbitals=='sp2':
            proj=[0, 1, 2]
        elif orbitals=='d':
            if self.itype==0:
                proj=[2]
            else:
                proj=[4, 5, 6, 7, 8]
        elif orbitals=='dxy':
            proj=[4]
        elif orbitals=='dxz':
            proj=[7]
        elif orbitals=='dyz':
            proj=[5]
        elif orbitals=='dz2':
            proj=[6]
        elif orbitals=='dx2':
            proj=[8]
        elif orbitals=='all':
            proj=range(self.nproj)
        else:
            print('Orbital choice %s is not valid.' % orbitals)
            return
        return proj


class data_from_db(procar_data):
    """
    Method for initializing PROCAR data class from database.
    """
    

    def __init__(self, db_data):
        self.energies = db_data['energies']
        self.filename = db_data['filename']
        self.itype = db_data['itype']
        self.kpointr = db_data['kpointr']
        self.kpoints = db_data['kpoints']
        self.nbands_down = db_data['nbands_down']
        self.nbands_up = db_data['nbands_up']
        self.nions = db_data['nions']
        self.nkpoints = db_data['nkpoints']
        self.noncollinear = db_data['noncollinear']
        self.nproj = db_data['nproj']
        self.nspin = db_data['nspin']
        self.occupancies = db_data['occupancies']
        self.phases = db_data['phases']
        self.projections = db_data['projections']
        self.weights = db_data['weights']
