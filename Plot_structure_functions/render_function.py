from ase.io import read, write
import os
from ase.data import covalent_radii
from ase.data.colors import jmol_colors
import numpy as np

def render(atoms):
	name = 'Fig'
	for j in range(1, len(atoms.split('_'))):
		name += '_' + atoms.split('_')[j]
		j += 1

	atoms = read(atoms)
	cell = atoms.get_cell()
	n = len(atoms)

	indice_C = [atom.index for atom in atoms if atom.symbol == 'C']
	indice_H = [atom.index for atom in atoms if atom.symbol == 'H']
	indice_O = [atom.index for atom in atoms if atom.symbol == 'O']
	indice_M = [atom.index for atom in atoms if atom.symbol == 'V']
	indice_Bi = [atom.index for atom in atoms if atom.symbol == 'Bi']
	indice_H = [atom.index for atom in atoms if atom.symbol == 'H']

	numbers = atoms.get_atomic_numbers()
	tex = []
	for l in range(len(numbers)):
		if numbers[l] in [1]:
			tex.append('ase3')
		else:
			tex.append('intermediate')

	radii = []
	for l in range(len(numbers)):
		if numbers[l] == 1:
			radii.append(0.45)
		elif numbers[l] in [6,8]:
			radii.append(covalent_radii[numbers[l]] / 0.95)
		else:
			radii.append(covalent_radii[numbers[l]] / 1.1)


	colors=jmol_colors[numbers]
	for i in indice_H:
		colors[i] = [148,255,255]/np.array([255.])
	for k in indice_C:
		colors[k] = [0.72, 0.42, 0.26]
	
	xmin = -1
	xmax = cell[0,0] * 1.5

	ymin = -1
	ymax = cell[1,1] + 1.0

	zmin = min([atom.z for atom in atoms])-1
	zmax = zmin + 12

	for view in ['top','side']:
		kwargs = {}
		kwargs['textures']=tex
		kwargs['show_unit_cell']=False
		kwargs['colors']=colors
		kwargs['radii']=radii
		kwargs['transparent']=False
		if view == 'top':
			#kwargs['bbox']=(xmin,ymin,xmax,ymax)
			kwargs['canvas_width']=1000
			kwargs['rotation']='0x, 0y, 0z'
			write('%s_%s.pov' % (name, view), atoms, run_povray=True, display = False, **kwargs)
		else:
			kwargs['canvas_width']=1000
			#kwargs['bbox']=(xmin+1,zmin,xmax,zmax) 
			kwargs['rotation']='-90x, -90y, 0z'
			kwargs['bbox']=(ymin, zmin, ymax, zmax)
			write('%s_%s.pov' % (name, view), atoms, run_povray=True, display = False, **kwargs)
			os.system('convert -border 8 -bordercolor black %s_%s.png %s_%s_k.png' % (name, view, name, view))
	os.system('rm *.pov *.ini')
	return
