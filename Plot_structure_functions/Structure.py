import numpy as np
import pandas as pd
import os, sys
from ase import Atoms
from ase.io import read, write
#from ase.visualize import view
from random import sample
from Data_manipulator import Affinity

class str_gen:
	# generate the terminated MXenes with O, OH and F groups
	def __init__(self, filename='ini.traj'):
		# Use the O terminated MXenes as the input
		self.filename=filename

	def F_gen(self, n_top, n_bot, I):
		for i in range(I):
			in_atom = read(self.filename)
			tot_num = len(in_atom)

			z_mid = [atom.z for atom in in_atom if atom.symbol == 'C'][0]

			top_O = [atom.index for atom in in_atom if atom.z > z_mid and atom.symbol == 'O']
			bot_O = [atom.index for atom in in_atom if atom.z < z_mid and atom.symbol == 'O']
			top_F = sample(top_O, n_top)
			#print (top_F)
			bottom_F = sample(bot_O, n_bot)        
			for j in range(len(top_F)):
				in_atom[top_F[j]].symbol = 'F'
			for k in range(len(bottom_F)):
				in_atom[bottom_F[k]].symbol = 'F'
				#view(in_atom)
			in_atom.write('con_upF%02i_botF%02i_%02i.traj' % (n_top, n_bot, i))
			
		print ("Structures generated!")
		return

	def F_site(self, n_top, n_bot, I):
		in_atom = read(self.filename)

		z_mid = [atom.z for atom in in_atom if atom.symbol == 'C'][0]

		top_O = [atom.index for atom in in_atom if atom.z > z_mid and atom.symbol == 'O']
		top_H = [atom.index for atom in in_atom if atom.z > z_mid and atom.symbol == 'H']
		bot_O = [atom.index for atom in in_atom if atom.z < z_mid and atom.symbol == 'O']
		bot_H = [atom.index for atom in in_atom if atom.z < z_mid and atom.symbol == 'H']
		O_avail_top = []
		O_avail_bot = []
		
		for i in range(len(top_O)):
			dis = []
			for j in range(len(top_H)):
				dis_i = in_atom.get_distance(top_O[i], top_H[j])
				dis.append(dis_i)
			if min(dis) > 1:
				O_avail_top.append(top_O[i])
		
		for i in range(len(bot_O)):
			dis = []
			for j in range(len(bot_H)):
				dis_i = in_atom.get_distance(bot_O[i], bot_H[j])
				dis.append(dis_i)
			if min(dis) > 1:
				O_avail_bot.append(bot_O[i])
		
		for i in range(I):
			in_atom = read(self.filename)
			top_F = sample(O_avail_top, n_top)
			#print (top_F)
			for j in top_F:
				in_atom[j].symbol = 'F'

			bot_F = sample(O_avail_bot, n_bot)
			#print (bot_F)
			for k in bot_F:
				in_atom[k].symbol = 'F'
			#print ([atom.index for atom in in_atom if atom.symbol == 'F'])
			in_atom.write('con_upF%02i_botF%02i_%02i.traj' %(len(top_F), len(bot_F), i))
		print ('Structures generated!')

	def add_H(self, atoms, n):
		for n in range(0, n, 1):
			atoms.append('H')
		return atoms

	def OH_gen(self, m_top, m_bot, I):
		in_atom = read(self.filename)
		tot_num = len(in_atom)

		z_mid = [atom.z for atom in in_atom if atom.symbol == 'C'][0]

		top_O = [atom.index for atom in in_atom if atom.z > z_mid and atom.symbol == 'O']
		bot_O = [atom.index for atom in in_atom if atom.z < z_mid and atom.symbol == 'O']
		for i in range(I):
			top_H = sample(top_O, m_top)
			bottom_H = sample(bot_O, m_bot)
			
			atoms_new = str_gen().add_H(in_atom, len(top_H))
			H_indices = [atom.index for atom in atoms_new if atom.symbol == 'H']
			for j in range(len(H_indices)):
				atoms_new[H_indices[j]].position = in_atom[top_H[j]].position
				atoms_new[H_indices[j]].z += 0.97
			
			new_num = len(atoms_new)
			atoms_bot = str_gen().add_H(atoms_new, len(bottom_H))
			H_bot_indices = [atom.index for atom in atoms_bot if atom.z < 5]
			
			for k in range(len(H_bot_indices)):
				atoms_bot[H_bot_indices[k]].position = in_atom[bottom_H[k]].position
				atoms_bot[H_bot_indices[k]].z += -0.97

			atoms_bot.write('con_upOH%02i_botOH%02i_%02i.traj' %(m_top, m_bot, i))
			
			del atoms_bot[tot_num:]
			del atoms_new[tot_num:]
			i = i + 1
		
		print ("Generation finished!")
		return

	def termination(self, n_top, n_bot, m_top, m_bot, I):
		for i in range(I):
			in_atom = read(self.filename)
			tot_num = len(in_atom)
			
			z_mid = [atom.z for atom in in_atom if atom.symbol == 'C'][0]

			top_O = [atom.index for atom in in_atom if atom.z > z_mid and atom.symbol == 'O']
			bot_O = [atom.index for atom in in_atom if atom.z < z_mid and atom.symbol == 'O']
			
			top_F = sample(top_O, n_top)
			#print (top_F)
			bottom_F = sample(bot_O, n_bot)        
			for j in range(len(top_F)):
				in_atom[top_F[j]].symbol = 'F'
			for k in range(len(bottom_F)):
				in_atom[bottom_F[k]].symbol = 'F'
				#in_atom.write('ini_tmp.traj')
			top_O_new = [atom.index for atom in in_atom if atom.z > z_mid and atom.symbol == 'O']
			bot_O_new = [atom.index for atom in in_atom if atom.z < z_mid and atom.symbol == 'O']
			top_H = sample(top_O_new, m_top)
			bot_H = sample(bot_O_new, m_bot)

			atoms_new = str_gen().add_H(in_atom, len(top_H))
			H_indices = [atom.index for atom in atoms_new if atom.symbol == 'H']
			for l in range(len(H_indices)):
				atoms_new[H_indices[l]].position = in_atom[top_H[l]].position
				atoms_new[H_indices[l]].z += 0.97 
			new_num = len(atoms_new)
					
			atoms_bot = str_gen().add_H(atoms_new, len(bot_H))
			H_bot_indices = [atom.index for atom in atoms_bot if atom.z < 5]

			for k in range(len(H_bot_indices)):
				atoms_bot[H_bot_indices[k]].position = in_atom[bot_H[k]].position
				atoms_bot[H_bot_indices[k]].z += -0.97

			atoms_bot.write('con_F%02i_%02i_OH%02i_%02i_n%02i.traj' %(n_top, n_bot, m_top, m_bot, i))

			del atoms_bot[tot_num:]
			del atoms_new[tot_num:]
			
		print ('Generation finished!')
		return

	def massive_generator(self, n_top, n_bot, m_top, m_bot, I):
		for n in range(0, n_top):
			for m in range(0, n_bot):
				for j in range(0, m_top):
					for k in range(0, m_bot):
						for i in range(I):
							str_gen().termination(n,m,j,k,i)
		return

	def H_ad_site(self):
		atoms = read(self.filename)
		N = len(atoms)
		atoms.append('H')
		
		z_mid = [atom.z for atom in atoms if atom.symbol == 'C'][0]
		indices = [atom.index for atom in atoms if atom.z > z_mid and atom.symbol == 'O']
		H_indices = [atom.index for atom in atoms if atom.z > z_mid and atom.symbol == 'H']
		
		O_avail = []

		if H_indices == []:
			O_avail = indices
			for i in range(len(indices)):
				atoms[N].position = atoms[indices[i]].position 
				atoms[N].z += 0.97
				atoms.write('Aff_%02i_%s' % (i, self.filename))
		else:
			for i in range(len(indices)):
				dis = []
				for j in range(len(H_indices)):
					dis_i = atoms.get_distance(indices[i], H_indices[j])
					dis.append(dis_i)
				if min(dis) > 1:
					O_avail.append(indices[i])
					atoms[N].position = atoms[indices[i]].position
					atoms[N].z += 0.97
					atoms.write('Aff_%02i_%s' % (i,self.filename))

		print ('All adsorption sites are considered!')
		return

	def H_F_site(self):
		atoms = read(self.filename)
		N = len(atoms)
		atoms.append('H')
		
		z_mid = [atom.z for atom in atoms if atom.symbol == 'C'][0]
		indices = [atom.index for atom in atoms if atom.z > z_mid and atom.symbol == 'F']
		H_indices = [atom.index for atom in atoms if atom.z > z_mid and atom.symbol == 'H']

		for i in range(len(indices)):
			atoms[N].position = atoms[indices[i]].position
			atoms[N].z += 0.9
			atoms.write('Aff_F%02i_%s' % (i, self.filename))

		print ('All F atoms are considered!')
		return

def tot_name(file_dir):
    # Get full path of trajectory files
    Tot_Name=[]
    for files in os.listdir(file_dir):
        if os.path.splitext(files)[1] == '.traj':
            Tot_Name.append(os.path.join(path, files))
    return Tot_Name

def file_name(file_dir):
	# Get names of trajectory files
    File_Name=[]
    for files in os.listdir(file_dir):
        if os.path.splitext(files)[1] == '.traj':
            File_Name.append(os.path.splitext(files)[0])
        else:
        	print ('Wrong file type')
    return File_Name

def H_surf(path):
	dirs = os.listdir(path)
	Files_name = file_name(path)
	for m in range(len(dirs)):
	    surf_m = read(dirs[m])
	    N = len(surf_m)
	    surf_m.append('H')
	    z_mid = [atom.z for atom in surf_m if atom.symbol == 'C'][0]
	    indices = [atom.index for atom in surf_m if atom.symbol == 'O' and atom.z > z_mid]
	    H_ind = [atom.index for atom in surf_m if atom.symbol == 'H' and atom.z > z_mid]
	    O_avail = []
	    for i in range(len(indices)):
	    	dis = []
	    	if H_ind == []:
	    		O_avail = indices
	    		
	    	else:
	    		for j in range(len(H_ind)):
	    			dis_i = surf_m.get_distance(indices[i], H_ind[j])
	    			dis.append(dis_i)
	    		if min(dis) > 1:
	    			O_avail.append(indices[i])
	    				#print (O_avail)

	    surf_m[N].position = surf_m[sample(O_avail, 1)[0]].position
	    surf_m[N].z += 0.97
	    surf_m.write('%s_Aff.traj'% Files_name[m])
	return
		
class Affinity_calc:
	def __init__(self, framefile, surffile, H_surffile):
		self.framefile = framefile
		self.surffile = surffile
		self.H_surffile = H_surffile
		return

	def frame(self, framefile, n_top, n_bot, m_top, m_bot, I):
		f = open(self.framefile, 'a')
		i = I
		line1 = '#NO_F'+'\t'+'NO_OH'+'\t'+'Top_F'+'\t'+'Bot_F'+'\t'+'Top_OH'+'\t'+'Bot_OH'+'\n'
		f.writelines(line1)
		for n in range(0, n_top):
			for m in range(0, n_bot):
				for j in range(0, m_top):
					for k in range(0, m_bot):
						for i in range(I):
							pre = str(m+n)+'\t'+str(j+k)+'\t'+str(n)+'\t'+str(m)+'\t'+str(j)+'\t'+str(k)+'\n'
							f.writelines(pre)
		f.close()
		return

	def Out_file(self):
		Frame = open(self.framefile)
		fs = open(self.surffile)
		fh = open(self.H_surffile)

		c_F = []
		c_OH = []
		F_top = []
		F_bot = []
		OH_top = []
		OH_bot = []

		for line1 in Frame:
			line1 = line1.strip()
			if line1.startswith('#'):continue
			c_F_i, c_OH_i = float(line1.split()[0]), float(line1.split()[1])
			F_top_i, F_bot_i = float(line1.split()[2]), float(line1.split()[3])
			OH_top_i, OH_bot_i = float(line1.split()[4]), float(line1.split()[5]) 
			c_F.append(c_F_i)
			c_OH.append(c_OH_i)
			F_top.append(F_top_i)
			F_bot.append(F_bot_i)
			OH_top.append(OH_top_i)
			OH_bot.append(OH_bot_i)

		E_surf = []
		E_H_surf = []

		for line2 in fs:
			line2 = line2.strip()
			E_surf_i = float(line2.split()[0])
			E_surf.append(E_surf_i)

		for line3 in fh:
			line3 = line3.strip()
			E_H_surf_i = float(line3.split()[0])
			E_H_surf.append(E_H_surf_i)

		Aff = Affinity(np.array(E_H_surf), np.array(E_surf))

		out = open('output.dat', 'a')
		outline_1 = 'NO_F'+'\t'+'NO_OH'+'\t'+'Top_F'+'\t'+'Bot_F'+'\t'+'Top_OH'+'\t'+'Bot_OH'+'\t'+'Aff'+'\n'
		out.writelines(outline_1)
		for i in range(len(Aff)):
			pre = str(c_F[i])+'\t'+str(c_OH[i])+'\t'+str(F_top[i])+'\t'+str(F_bot[i])+'\t'+str(OH_top[i])+'\t'+str(OH_bot[i])+'\t'+str(Aff[i])+'\n'
			out.writelines(pre)
		Frame.close()
		fs.close()
		fh.close()
		out.close()

		path = 'output.dat'
		df = pd.read_table(path, sep='\t')

		df_a = df[['NO_F','NO_OH','Aff']].groupby(['NO_F','NO_OH'])
		Agg_a = df_a.agg({'Aff':[np.mean, np.max, np.min, np.std]})
		Agg_a.to_csv('Mean.txt')
		return

# scale CONTCARS into the same cell
def scale(file_1, file_2):
	atoms0 = read(file_1)
	scaled0 = atoms0.get_scaled_positions(wrap=False)
	
	atoms = read(file_2)
	scaled = atoms.get_scaled_positions(wrap=False)
	for m in range(len(scaled)):
		for i in range(3):
			if scaled[m,i] - scaled0[m,i] > 0.9:
				scaled[m,i] -= 1.
			elif scaled[m,i] - scaled0[m,i] < -0.9:
				scaled[m,i] += 1.
	atoms.set_scaled_positions(scaled)
	atoms.write(file_2)
	return 