import numpy as np
import os
from pandas import Series, DataFrame

def file_name(file_dir):
	#split the file name before .traj#
    File_Name=[]
    for files in os.listdir(file_dir):
        if os.path.splitext(files)[1] == '.traj':
            File_Name.append(os.path.splitext(files)[0])
    return File_Name

def tot_name(file_dir):
	#get total path for each traj files#
    Tot_Name=[]
    path = os.getcwd()
    for files in os.listdir(file_dir):
        if os.path.splitext(files)[1] == '.traj':
            Tot_Name.append(os.path.join(path, files))
    return Tot_Name

def lin_fit(x,A,B):
    return A*x + B

def get_MAE(y_test, y_true):
    return np.sum(np.absolute(y_test - y_true))/len(y_test)

def Affinity(Adsorbed_H, Surface):
    E_H2O = -12.52828665
    E_O2 = -6.74526276
    return Adsorbed_H - Surface - 0.5* E_H2O + 0.25 * E_O2

def E_ad(E_tot, E_surf, E_mol):
    E_ad = np.array(E_tot) - np.array(E_surf) - np.array(E_mol)
    return E_ad

def Gibbs(E, H, TS):
    G = np.array(E) + np.array(H) - np.array(TS)
    return G

def kJ2eV(kJ):
    eV = np.array(kJ) * 1.0364 * 10 ** (-2)
    return eV

def bohr2A(chg_bohr):
    chg_A = 0.529 ** 3 * chg_bohr
    return chg_A
    
from scipy.special import comb
#calculate the probability with partition function# 
def Prob(n, m):
    Access = 0
    weight = comb(16, n) * comb(16, m)
    if n + m == 0:
        Prob = 1
    if 0 < n + m < 16:
        for i in range(0, n + m + 1):
            Access += comb(16, i) * comb(16, n+m-i)
            Prob = weight / Access
    if 16 <= n + m:
        for i in range(0, 16):
            Access += comb(16, i) * comb(16, n + m -i) 
            Prob = weight / Access
    return (Prob)

# classification the data based on the difference (threshold)
def cluster(Data_set, threshold):
    stand_array = np.asarray(Data_set).ravel('C')
    stand_Data = Series(stand_array)
    index_list, class_k = [], []
    while stand_Data.any():
        if len(stand_Data) == 1:
            index_list.append(list(stand_Data.index))
            class_k.append(list(stand_Data))
            stand_Data = stand_Data.drop(stand_Data.index)
        else:
            class_data_index = stand_Data.index[0]
            class_data=stand_Data[class_data_index]
            stand_Data=stand_Data.drop(class_data_index)
            if (abs(stand_Data-class_data)<=threshold).any():
                args_data=stand_Data[abs(stand_Data-class_data)<=threshold]
                stand_Data=stand_Data.drop(args_data.index)
                index_list.append([class_data_index]+list(args_data.index))
                class_k.append([class_data]+list(args_data))
            else:
                index_list.append([class_data_index])
                class_k.append([class_data])
    return index_list,class_k

# calculate the angle between two vectors
def Calc_angle(x, y):
    a = np.array(x)
    b = np.array(y)
    la = np.sqrt(a.dot(a))
    lb = np.sqrt(b.dot(b))
    cos_alpha = a.dot(b) / (la * lb)
    alpha_d =np.arccos(cos_alpha)
    alpha_r = alpha_d * 180 / np.pi
    print ('cell parameter: %.3f /AA' %la)
    print ('cell parameter: %.3f /AA' %lb)
    print ('angle: %.3f' %alpha_r)
    return

