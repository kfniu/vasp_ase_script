from ase import *
from ase.io import read, write
from ase.constraints import *
import numpy as np

def Rotate(atoms, atom_list = [0], new_origin = (0,0,0), axis_atom = (0,0,0), angle = 0):
  """
  This script is to give a rotation of the selected atom for a given axis and give angle
  There are 4 input parameters required: 1. A list of atoms to be rotated
                                         2. The coordinate of the new origin point (a numpy array)
                                         3. A point P, indicating the axis or rotation (in old coordinates) (a numpy array)
                                         4. The angle of rotation (in degree)
  """
  
  """ The following are input parameters"""
  atoms = atoms
  List = atom_list#range(96,102) # a list of atoms to be rotated
  New_Origin = new_origin #atoms[97].position.copy() # the new origin point for rotation
  P = axis_atom #atoms[96].position.copy() # the axis of rotation
  Angle = angle #30

  """ code starts from here """
  vector = P - New_Origin
  length = np.sqrt(np.sum(vector**2))
  axis = vector / length
  ux,uy,uz = axis
  
  Angle *= np.pi / 180.
  s = np.sin(Angle)
  c = np.cos(Angle)
  
  array = np.array([[ux**2 + (1-ux**2)*c, ux*uy*(1-c) - uz*s, ux*uz*(1-c) + uy*s],
                    [ux*uy*(1-c) + uz*s, uy**2 + (1-uy**2)*c, uy*uz*(1-c) - ux*s],
                    [ux*uz*(1-c) - uy*s, uy*uz*(1-c) + ux*s, uz**2 + (1-uz**2)*c]])
  
  matrix = np.mat(array)
  
  atoms.positions -= New_Origin
  for i in List:
    pos = atoms[i].position.copy()
    posmat = np.mat(pos)
    newpos = posmat * matrix
    atoms[i].position = newpos.copy()[0]
  atoms.positions += New_Origin

  """ code finishes, here you can write output """
  return(atoms)



#---------------------------------------------------------------
#  atom_sort_xyz function
#  Thsi function reorder the system according to atom positions
#  (increasing order of x,y,z)
#  e.g. newatoms = sort_atom(atoms)
#---------------------------------------------------------------
def sort_atom(atoms):
  atom_list = []
  top_down_list = []
  zmin = 0.0
  zmax = 0.0
  for a in atoms:
    if a.z < zmin:
      zmin = a.z
    elif a.z > zmax:
      zmax = a.z

  for atom in atoms:
    assert(zmax >= atom.z >= zmin)
    atom_pos = np.round(atom.position.copy(),2)
    temp = (atom_pos[2],atom_pos[1],atom_pos[0],atom.index)
    atom_list.append(temp)

  atom_list.sort()

  for a in atom_list:
    top_down_list.append(a[3])

  new_atoms = atoms[top_down_list].copy()
  return new_atoms
#--------------------------------------------------


#---------------------------------------------------------------
#  sort_element function
#  This function reorder the system according to atom type
#  (increasing order of x,y,z)
#  e.g. newatoms = sort_atom(atoms)
#---------------------------------------------------------------
def sort_element(atoms):
  atom_list = []
  e_list = []
  for atom in atoms:
    temp = (atom.symbol, atom.index)
    atom_list.append(temp)
  atom_list.sort()
  for a in atom_list:
    e_list.append(a[1])

  new_atoms = atoms[e_list].copy()

  return new_atoms


#-----------------------------------------------------------
# This function read ASE atoms and write the .in file for 
# Quantum Espresso pw.x file for structure optimizations
# To use: Newatoms = get_pw_relax(atoms), a new file named
# 'relax.in' will be generated in the working directory
# You need worry (1) the pseudopotentials (2) K_POINTS
#-----------------------------------------------------------
def get_pw_relax(atoms,filename='relax.in'):
  atoms = sort_element(atoms)

  if atoms.constraints:
    sflags = np.zeros((len(atoms), 3), dtype=bool)
    for constr in atoms.constraints:
        if isinstance(constr, FixScaled):
            sflags[constr.a] = constr.mask
        elif isinstance(constr, FixAtoms):
            sflags[constr.index] = [True, True, True]

  fd = open(filename, 'w')

  ### write the flags in quantum espresso pw relax
  fd.write('&CONTROL\n',)
  fd.write(' prefix = "HMo",\n')
  fd.write(' pseudo_dir = "/home/haiping/Codes/QE_PSEUDO",\n')
  fd.write(' outdir      = "./Out",\n')
  fd.write(' nstep       = 100,\n')
  fd.write(' tstress     = .true.,\n')
  fd.write(' tprnfor     = .true.,\n')
  fd.write(' wf_collect  = .true.,\n')
  fd.write(' etot_conv_thr = 1.0D-4,\n')
  fd.write(' forc_conv_thr = 1.0D-3,\n')
  fd.write('/\n')
  fd.write('&SYSTEM\n')
  fd.write(' ibrav     = 0,\n')
  fd.write(' celldm(1) = 1.889716,\n')
  ##################################################
  natoms = len(atoms)
  symbols = atoms.get_chemical_symbols()
  sc = []
  psym = symbols[0]
  count = 0
  for sym in symbols:
      if sym != psym:
          sc.append((psym,count))
          psym = sym
          count = 1
      else:
          count += 1
  sc.append((psym, count))
  nelements = len(sc)
  fd.write(' nat       = %i,\n'%natoms)
  fd.write(' ntyp      = %i,\n'%nelements)
  ##################################################
  fd.write(' ecutwfc   = 24.D0,\n')
  fd.write(' ecutrho   = 144.D0,\n')
  fd.write(' occupations = "smearing",\n')
  fd.write(' smearing    = "methfessel-paxton",\n')
  fd.write(' degauss     = 0.01D0,\n')
  fd.write(' nosym       = .true.,\n')
  fd.write(' tot_charge  = 0.0d0,\n')
  fd.write('/\n')
  fd.write('&ELECTRONS\n')
  fd.write(' conv_thr    = 1.D-7,\n')
  fd.write(' diagonalization="david",\n')
  fd.write(' mixing_beta = 0.7D0,\n')
  fd.write(' electron_maxstep = 999,\n')
  fd.write('/\n')
  fd.write('&IONS\n')
  fd.write(' ion_dynamics = "bfgs",\n')
  fd.write(' bfgs_ndim   = 3,\n')
  fd.write('/\n')
  fd.write('CELL_PARAMETERS {angstrom}\n')
  ##################################################
  for vec in atoms.get_cell():
     #fd.write(' ')
     for el in vec:
         fd.write(' %11.7f' % el)
     fd.write('\n')
  ################################################## 
  fd.write('ATOMIC_SPECIES\n')
  for a in sc: 
    index = int(a[1])-1
    mass = atoms.get_masses()[index]
    fd.write('%2s'%a[0])
    fd.write('%9.3f'%mass)
    fd.write('    ?.UPF\n')
  ##################################################
  fd.write('ATOMIC_POSITIONS {angstrom}\n')
  coord = atoms.get_positions()
  cform = ' %10.6f'
  for iatom, atom in enumerate(coord):
    symbol = atoms[iatom].symbol
    fd.write('%2s'%symbol)
    for dcoord in atom:
        fd.write(cform % dcoord)
    if atoms.constraints:
        for flag in sflags[iatom]:
            if flag:
                s = '0'
            else:
                s = '1'
            fd.write('%4s' % s)
    fd.write('\n')  
  ###################################################   
  fd.write('K_POINTS {Gamma}\n')
  fd.write('EOF')
  fd.close()
  return atoms
