import sys
import numpy as np

class dos_file:
	#split the dos file into several lists for further visualization.
	#calculate the center of the given orbital.
	def __init__(self, name):
		self.name = name

	def dos_list(self, emin, emax, nspin):
		f = open(self.name, 'r')
		if nspin == 1:
			Sum = 0
			fill = 0
			dos = []
			E = []
			for line in f:
				if line.startswith('#'):continue
				E_i, dos_i = float(line.split()[0]), float(line.split()[1])
				dos.append(dos_i)
				E.append(E_i)

				if emin < E_i < emax:
					Sum += E_i * dos_i
					fill += dos_i
			center = Sum / fill	
			#print ('Center of DOS is: %.5f eV' % center)
			return E, dos, center

		elif nspin == 2:
			Sum = 0
			fill = 0
			filling = 0
			E = []
			dos_up = []
			dos_down = []
			for line in f:
				line = line.strip()
				if line.startswith('#'):continue
				E_i, dos_up_i, dos_down_i = float(line.split()[0]), float(line.split()[1]), float(line.split()[2])
				E.append(E_i)
				dos_up.append(dos_up_i)
				dos_down.append(dos_down_i)

				if emin < E_i < emax:
					Sum += E_i * (dos_up_i - dos_down_i)
					fill += (dos_up_i - dos_down_i)
			center = Sum / fill
			#print ('Center of DOS is: %.5f eV' % center)
			return E, dos_up, dos_down, center
		else:
			print ('Wrong spin-polarization!')
			sys.exit(0)
		f.close()

	def width(self, emin, emax, nspin=2):
		energy = dos_file(self.name).dos_list(emin, emax, nspin)[0]
		dos_up = dos_file(self.name).dos_list(emin, emax, nspin)[1]
		dos_down = dos_file(self.name).dos_list(emin, emax, nspin)[2]
		center = dos_file(self.name).dos_list(emin, emax, nspin)[3]

		second_order_sum = 0
		dos_sum = 0
		for i in range(len(energy)):
			if emin < energy[i] < emax:
				second_order_sum += (energy[i] - center) ** 2 * (dos_up[i] - dos_down[i])
				dos_sum += (dos_up[i] - dos_down[i])
			else:
				pass
		width = 4 * np.sqrt(second_order_sum / dos_sum)
		
		return width
		f.close()


def read_COHP(name):
	f = open(name, 'r')
	E = []
	COHP = []

	for line in f:
		line = line.strip()
		if line.startswith('#'):continue

		E_i = float(line.split()[0])
		COHP_i = float(line.split()[1])
		E.append(E_i)
		COHP.append(COHP_i)
	return E, COHP
	f.close()
		


