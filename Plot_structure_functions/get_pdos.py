from screening_kit.file_manipulators.procar import procar_data
import numpy as np
import time

# Change these
E_F = -1.0142 # Fermi energy
#E_vac = 3.696

emin = -5. # minimum energy
emax = 5 # maximum energy
npts = 10001 # number of points

width = 0.1 # smearing

#######

procar = procar_data('PROCAR', nspin=1) # nspin=2 if spin-polarized 

indices = [213] # indices of atoms range(len(atoms))

energies, pdos_px_up = procar.get_partial_dos(emin,emax,npts,width,energy_shift=-E_F,lineshape="Gaussian",spins=[0], kpoints=[0], atoms=indices, orbitals='px').T

np.savetxt('pdos.dat' % extension, np.array([x, pdos]).T, '%.4f %.7f')
