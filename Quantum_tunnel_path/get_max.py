from ase.lattice.surface import fcc111, add_adsorbate
from ase.io import read, write
from ase import units
import numpy as np
from ase.calculators.vasp import Vasp, xdat2traj
import os

calc = Vasp(prec='Normal',
            xc='PBE',
            gamma=True,
            kpts=[1,1,1],
            encut=400,
            ediff=1E-4,
#            nelmin=5,
            algo='Fast',
            lreal='Auto',
            ismear=1,
            sigma=0.1,
#            icharg=1,
            lwave=True,
            lcharg=False,
            ibrion=-1,
            nsw=0,
            luse_vdw=True,
            zab_vdw = -1.8867,
            aggac=0.0,
            gga='MK',
            param1=0.1234,
            param2=0.7114,
            )


#atoms_is = read('POSCAR_IS-start')
#atoms_fs = read('POSCAR_FS-start')

atoms_is = read('IS_calculations/IS.traj@0')
atoms_fs = read('FS_calculations/FS.traj@0')

#os.mkdir('IS_calculations')
#os.mkdir('FS_calculations')


#atoms_is.set_calculator(calc)
#atoms_fs.set_calculator(calc)

T = 200

masses = atoms_is.get_masses()

i = 0
while i < 1:
    # IS calculation
    E_is = atoms_is.get_potential_energy()
    F_is = atoms_is.get_forces()

    # FS calculation
    E_fs = atoms_fs.get_potential_energy()
    F_fs = atoms_fs.get_forces()

    # Not it is time to update positions
    E_ref = np.min([E_is, E_fs])
    E_is -= E_ref
    E_fs -= E_ref

    is_boltzmann = np.exp(-E_is / (units.kB * T))
    fs_boltzmann = np.exp(-E_fs / (units.kB * T))
    Z = is_boltzmann + fs_boltzmann

    is_scale = fs_boltzmann / Z
    fs_scale = is_boltzmann / Z
    
    force = is_scale * F_is + fs_scale * F_fs
    
    H = force
    for n, m in enumerate(masses):
        H[n] /= m
    dr = H
    if np.linalg.norm(dr) > 0.01:
        dr /= np.linalg.norm(dr) / 0.01
    print np.linalg.norm(dr)
    
    atoms_is.positions += dr
    atoms_fs.positions += dr
    i += 1

