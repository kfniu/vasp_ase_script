#!/usr/bin/env python

from ase.io import read
import numpy as np
import sys

file1 = sys.argv[1].lstrip()
file2 = sys.argv[2].lstrip()


atoms1 = read(file1)
atoms2 = read(file2)

indices = [143]

for index in indices:
    R = atoms1[index].position - atoms2[index].position
    
    print(index, np.linalg.norm(R))
