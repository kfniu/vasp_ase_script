from ase.io import read, write
from ase import units
import numpy as np
from ase.calculators.vasp import Vasp, xdat2traj
from tunneling_turning_points import tunneling_turning_points
from ase.optimize import LBFGS, BFGS, FIRE

atoms_is = read('POSCAR_IS-start')
atoms_fs = read('POSCAR_FS-start')

# You might want to restart the calculation. I usually rename the IS.traj and FS.traj before restarting just to keep the full trajectory.
# Beware that in case a calculation crashed (due to time limitation or something else) the last configuration of IS.traj may not correspond 
# to the last configuration of FS.traj because for each step the code first run the calculation for IS and then the calculation for FS. 

#atoms_is = read('IS_calculations/IS-01.traj')
#atoms_fs = read('FS_calculations/FS-01.traj')

tunneling_indices = [143] # Index of tunneling hydrogen atom

R = atoms_fs[tunneling_indices].positions - atoms_is[tunneling_indices].positions 

atoms = atoms_is.copy()
atoms.positions = (atoms_is.positions + atoms_fs.positions) / 2

vasp_calc = Vasp(prec='Normal',
                 xc='PBE',
                 gamma=True,
                 kpts=[1,1,1],
                 encut=400,
                 ediff=1E-4,
                 algo='Fast',
                 lreal='Auto',
                 ismear=1,
                 sigma=0.1,
                 lwave=True,
                 lcharg=False,
                 ibrion=-1,
                 nsw=0,
                 luse_vdw=True,
                 zab_vdw = -1.8867,
                 aggac=0.0,
                 gga='MK',
                 param1=0.1234,
                 param2=0.7114,
                 )


calc = tunneling_turning_points(R, tunneling_indices, vasp_calc, T=150, N_rot=1, Rmax=[0.8])
#calc = tunneling_turning_points(R, tunneling_indices, vasp_calc, T=150, N_rot=1, Rmax=None)
atoms.set_calculator(calc)

dyn = BFGS(atoms, trajectory='opt-01.traj')
dyn.run(fmax=0.01, steps=2000)
