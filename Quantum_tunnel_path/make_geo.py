from ase.io import read
import numpy as np

Rmax = 0.8
ind_H = 143

images = read('neb_path.traj@:')

atoms_is = images[10].copy()
atoms_fs = images[12].copy()

H_is = atoms_is[ind_H].position
H_fs = atoms_fs[ind_H].position

H_com = (H_is + H_fs) / 2
R = H_fs - H_is

R_new = Rmax * R / np.linalg.norm(R)

H_is_new = H_com - R_new / 2
H_fs_new = H_com + R_new / 2

atoms_is.positions[ind_H] = H_is_new
atoms_fs.positions[ind_H] = H_fs_new

atoms_is.write('POSCAR_IS-start')
atoms_fs.write('POSCAR_FS-start')
